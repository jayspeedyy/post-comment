const router = require('express').Router();
const User = require('../Model/User');
const jwt = require('jasonwebtoken');

router.post('/register', async(req, res) => {
    
    const user = new User ({
         name    : req.body.name,
         email   : req.body.email,
         password: req.body.password
     });
     try{
        const token = jwt.sign ({_id: user._id}, process.env.SECRET_KEY); 
        res.header('auth-token', token).send(token);
        return res.send(user);
        console.log('line 14');
        console.log('Saveduser', saveduser);
     }catch(error){
         res.status(400);
     }
});

/*router.post('/login', async(req,res) => {
});*/


module.exports = router;